<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
   $get = "http://10.193.70.129/api/individus";
    //return "get all data is'$get'";
    return view('index');
});
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
 

  

    $router->get('/individus', 'individucontroller@index');
    $router->post('/individu', 'individucontroller@create');
    $router->get('/individu/{id}', 'individucontroller@show');
    $router->put('/individu/{id}', 'individucontroller@update');
    $router->delete('/individu/{id}', 'individucontroller@destroy');
    
 });
